#Création du fichier éxecutable 'affichage_table'
bin/affichage_table: obj/affichage.o obj/calculatoire.o obj/main.o
	g++ $(shell pkg-config --libs libpq) -o $@ $^

#Création du fichier objet 'main.o'
obj/main.o: src/main.cpp
	g++ -c $(shell pkg-config --cflags libpq) -o $@ $^

#Création du fichier objet 'calculatoire.o'
obj/calculatoire.o: src/calculatoire.cpp
	g++ -c $(shell pkg-config --cflags libpq) -o $@ $^

#Création du fichier objet 'affichage.o'
obj/affichage.o: src/affichage.cpp
	g++ -c $(shell pkg-config --cflags libpq) -o $@ $^

#Supprime tout ce qui est dans bin/ et obj/
clean:
	rm bin/* obj/*


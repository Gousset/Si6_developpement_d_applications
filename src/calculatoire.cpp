#include "calculatoire.h"
#include<cstring>

int champLePlusLong(PGresult *res, int plusGrandChamp)
{
  for(int j = 0; j < PQnfields(res); j++)
  {
    if(strlen(PQfname(res, j)) > plusGrandChamp)
    {
      plusGrandChamp = std::strlen(PQfname(res, j));
    }
  }

  return plusGrandChamp;
}


#include <libpq-fe.h>
#include <iostream>
#include <iomanip>
using namespace std;

void ligneSeparatrice(int champ, int plusGrandChamp)
{
  for(int k = 0; k < (champ * plusGrandChamp); k++)
  {
    cout <<  "-";
  }
}

int main()
{
  //Ping
  if(PQping("host=postgresql.bts-malraux72.net port=5432 dbname=c.gousset user=c.gousset") == PQPING_OK)
  {
    //Connexion
    PGconn *connexion;
    connexion = PQconnectdb("host=postgresql.bts-malraux72.net port=5432 dbname=c.gousset user=c.gousset");

    if(PQstatus(connexion) == CONNECTION_OK)
    {
      cout << "La connexion au serveur de base de donnée '" << PQhost(connexion) << "a été établie avec les paramètres suivants" << endl;
      cout << endl << "* Utilisateur : " << PQuser(connexion) << endl;
      cout << "* Mot de passe : ";

      for(int i = 0; i < sizeof(PQpass(connexion)); i++)
      {
        cout << "*";
      }

      cout << endl << "* Base de données :" << PQdb(connexion) << endl;
      cout << "* Port TCP :" << PQport(connexion) << endl;
      cout << "* Chiffrement SSL :";

      if(PQconnectionUsedPassword(connexion) == 1)
      {
        cout << "true" << endl;
      }
      else
      {
        cout << "false" << endl;
      }

      cout << "* Encodage : " << PQparameterStatus(connexion, "server_encoding") << endl;
      cout << "* Version du protocole : " << PQprotocolVersion(connexion) << endl;
      cout << "* Version du serveur : " << PQparameterStatus(connexion, "server_version") << endl;
      cout << "* Version de la bibliothèque 'libpq' du client : " << PQlibVersion() << endl;
      PGresult *co;
      co = PQexec(connexion, "SELECT * FROM si6.\"Animal\" INNER JOIN si6.\"Race\" ON (si6.\"Animal\".race_id = si6.\"Race\".id) WHERE sexe = 'Femelle' AND si6.\"Race\".nom = 'Singapura';");

      if(PQresultStatus(co) ==  PGRES_TUPLES_OK || PQresultStatus(co) == PGRES_COMMAND_OK)
      {
        const char barre = '|', espace = ' ';
        int plusGrandChamp = 0;

        //Test pour trouver le champs le plus grand
        for(int j = 0; j < PQnfields(co); j++)
        {
          if(sizeof(PQfname(co, j)) > plusGrandChamp)
          {
            plusGrandChamp = sizeof(PQfname(co, j));
          }
        }

        ligneSeparatrice(PQnfields(co), plusGrandChamp);
        cout << endl;

        //Affichage des noms des champs
        for(int k = 0; k < PQnfields(co); k++)
        {
          cout << barre << espace << left << setw(plusGrandChamp) << setfill(' ') << PQfname(co, k) << espace;
        }

        cout << barre << endl;
        //Affichage de la ligne de séparation
        ligneSeparatrice(PQnfields(co), plusGrandChamp);
        cout << endl;

        //Affichage des données
        for(int j = 0; j < PQntuples(co); j++)
        {
          for(int k = 0; k < PQnfields(co); k++)
          {
            /* if(PQfnumber ==  "description")
            {
            cout <<
            }*/
            cout << barre << espace << left << setw(plusGrandChamp) << setfill(' ') << PQgetvalue(co, j, k) << espace;
          }

          cout << barre << endl;
        }

        ligneSeparatrice(PQnfields(co), plusGrandChamp);
        cout << endl << "L'exécution de la requête SQL a retourné " << PQntuples(co) << " enregistrements" << endl;
        PQclear(co);
      }
      else
      {
        cout << "Erreur" << endl;
      }
    }
    else
    {
      cout << "Echec de connexion" << endl;
    }
  }

  return 0;
}


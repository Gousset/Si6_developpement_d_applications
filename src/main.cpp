#include <iostream>
#include <iomanip>
#include <limits>
#include <cstring>
#include <libpq-fe.h>

unsigned int largeur_maximum_nom_champ(PGresult *);
unsigned int largeur_maximum_tableau(PGresult *, const char = '|', const char = ' ');
void afficher_etat_connexion(PGconn *);
void afficher_ligne_separatrice(const char, unsigned int);
void afficher_en_tete(PGresult *, unsigned int = 0, const char = '|', const char = ' ');
void afficher_tuples(PGresult *, unsigned int = 0, const char = '|', const char = ' ');
void afficher_tableau(PGresult *, const char = '-', const char = '|', const char = ' ', const char = 'l');

int main()
{
  const char *connexion_infos;
  PGconn *connexion;
  connexion_infos = "host='postgresql.bts-malraux72.net' port=5432 dbname='c.gousset' user='c.gousset' connect_timeout=5";

  if(PQping(connexion_infos) == PQPING_OK)
  {
    // Établissement de la connexion avec les paramètres
    // fournis dans la chaîne de caractères 'connexion_infos'
    connexion = PQconnectdb(connexion_infos);

    if(PQstatus(connexion) == CONNECTION_OK)
    {
      // Affichage des informations de connexion
      afficher_etat_connexion(connexion);
      // Récupération des données
      const char *requete_sql = "SELECT si6.\"Animal\".id, si6.\"Animal\".nom as \"nom de l\'animal\", sexe, date_naissance AS \"date de naissance\", commentaires, si6.\"Race\".nom AS race, description FROM si6.\"Animal\" JOIN si6.\"Race\" on si6.\"Animal\".race_id = si6.\"Race\".id WHERE si6.\"Animal\".sexe = 'Femelle' AND si6.\"Race\".nom = 'Singapura';";
      PGresult *resultats = PQexec(connexion, requete_sql);
      ExecStatusType etat_execution = PQresultStatus(resultats);

      // Vérification de l'état des données reçues
      if(etat_execution == PGRES_TUPLES_OK)
      {
        // Exploitation des résultats
        afficher_tableau(resultats);
      }
      else if(etat_execution == PGRES_EMPTY_QUERY)
      {
        std::cerr << "La chaîne envoyée au serveur était vide." << std::endl;
      }
      else if(etat_execution == PGRES_COMMAND_OK)
      {
        std::cerr << "La requête SQL n'a renvoyé aucune donnée." << std::endl;
      }
      else if(etat_execution == PGRES_COPY_OUT)
      {
        std::cerr << "Début de l'envoi (à partir du serveur) d'un flux de données." << std::endl;
      }
      else if(etat_execution == PGRES_COPY_IN)
      {
        std::cerr << "Début de la réception (sur le serveur) d'un flux de données." << std::endl;
      }
      else if(etat_execution == PGRES_BAD_RESPONSE)
      {
        std::cerr << "La réponse du serveur n'a pas été comprise." << std::endl;
      }
      else if(etat_execution == PGRES_NONFATAL_ERROR)
      {
        std::cerr << "Une erreur non fatale (une note ou un avertissement) est survenue." << std::endl;
      }
      else if(etat_execution == PGRES_FATAL_ERROR)
      {
        std::cerr << "Une erreur fatale est survenue." << std::endl;
      }
      else if(etat_execution == PGRES_COPY_BOTH)
      {
        std::cerr << "Lancement du transfert de données Copy In/Out (vers et à partir du serveur)." << std::endl;
      }
      else if(etat_execution == PGRES_SINGLE_TUPLE)
      {
        std::cerr << "La structure PGresult contient une seule ligne de résultat provenant de la commande courante." << std::endl;
      }

      PQclear(resultats);
    }
    else
    {
      std::cerr << "Malheureusement la connexion n'a pas pu être établie" << std::endl;
    }

    PQfinish(connexion);
  }
  else
  {
    std::cerr << "Malheureusement le serveur n'est pas joignable. Vérifier la connectivité" << std::endl;
  }

  return 0;
}

unsigned int largeur_maximum_nom_champ(PGresult *donnees)
{
  unsigned int taille_champ = 0;
  unsigned int champ_maximum = std::numeric_limits<unsigned int>::min();
  char *nom_de_champ;

  // Recherche de la taille du nom de champ le plus large
  for(unsigned int compteur_champ = 0; (int)compteur_champ < PQnfields(donnees); ++compteur_champ)
  {
    nom_de_champ = PQfname(donnees, compteur_champ);
    taille_champ = std::strlen(nom_de_champ);

    if(champ_maximum < taille_champ)
    {
      champ_maximum = taille_champ;
    }
  }

  return champ_maximum;
}

unsigned int largeur_maximum_tableau(PGresult *donnees, const char separateur_champs, const char marge_separateur)
{
  unsigned int taille_separateur_champs = 1;
  unsigned int taille_marge_separateur = 1;

  if(separateur_champs == '\0')
  {
    taille_separateur_champs = 0;
  }

  if(marge_separateur == '\0')
  {
    taille_marge_separateur = 0;
  }

  return PQnfields(donnees) * (taille_separateur_champs + 2 * taille_marge_separateur + largeur_maximum_nom_champ((donnees))) + taille_separateur_champs;
}

void afficher_etat_connexion(PGconn *connexion)
{
  unsigned short int nb_caracteres;
  std::cout << "La connexion au serveur de base de données '" << PQhost(connexion)  << "' a été établie avec les paramètres suivants :" << std::endl;
  std::cout << " * utilisateur : " << PQuser(connexion) << std::endl;
  std::cout << " * mot de passe : ";
  nb_caracteres = sizeof PQpass(connexion);

  for(unsigned int i = 0; i < nb_caracteres; ++i)
  {
    std::cout << '*';
  }

  std::cout << " (<-- autant de '*' que la longueur du mot de passe)" << std::endl;
  std::cout << " * base de données : " << PQdb(connexion) << std::endl;
  std::cout << " * port TCP : " << PQport(connexion) << std::endl;
  std::cout << std::boolalpha << " * chiffrement SSL : " << (bool)(PQgetssl(connexion) != 0) << std::endl;
  std::cout << " * encodage : " << pg_encoding_to_char(PQclientEncoding(connexion)) << std::endl;
  std::cout << " * version du protocole : " << PQprotocolVersion(connexion) << std::endl;
  std::cout << " * version du serveur : " << PQserverVersion(connexion) << std::endl;
  std::cout << " * version de la bibliothèque 'libpq' du client : " << PQlibVersion() << std::endl;
}

void afficher_ligne_separatrice(const char symbole, unsigned int longueur)
{
  for(unsigned int numero_caractere = 0; numero_caractere < longueur; ++numero_caractere)
  {
    std::cout << symbole;
  }

  std::cout << std::endl;
}

void afficher_en_tete(PGresult *donnees, unsigned int largeur_champ, const char separateur_champs, const char marge_separateur)
{
  for(unsigned int compteur_champ = 0; (int)compteur_champ < PQnfields(donnees); ++compteur_champ)
  {
    std::cout << separateur_champs
              << marge_separateur
              << std::setw(largeur_champ)
              << PQfname(donnees, compteur_champ)
              << marge_separateur;
  }

  std::cout << separateur_champs << std::endl;
}

void afficher_tuples(PGresult *donnees, unsigned int largeur_champ, const char separateur_champs, const char marge_separateur)
{
  char *valeur_champ;

  for(unsigned int compteur_tuple = 0; (int)compteur_tuple < PQntuples(donnees); ++compteur_tuple)
  {
    for(unsigned int compteur_champ = 0; (int)compteur_champ < PQnfields(donnees); ++compteur_champ)
    {
      char *tmp;
      tmp = (char *)std::malloc(largeur_champ * sizeof(char) + 1);
      valeur_champ = PQgetvalue(donnees, compteur_tuple, compteur_champ);

      if(PQgetlength(donnees, compteur_tuple, compteur_champ) > (int)largeur_champ)
      {
        valeur_champ = std::strncpy(tmp, valeur_champ, largeur_champ);
        valeur_champ[largeur_champ] = 0;

        for(unsigned int i = 1; i <= 3; valeur_champ[largeur_champ - i++] = '.');
      }

      std::cout << separateur_champs
                << marge_separateur
                << std::setw(largeur_champ)
                << valeur_champ
                << marge_separateur;
      std::free(tmp);
    }

    std::cout << separateur_champs << std::endl;
  }
}

void afficher_tableau(PGresult *donnees, const char separateur_ligne, const char separateur_champs, const char marge_separateur, const char alignement)
{
  unsigned int largeur_ligne = largeur_maximum_tableau(donnees, separateur_champs, marge_separateur);
  unsigned int largeur_champ = largeur_maximum_nom_champ(donnees);

  if(alignement == 'l')
  {
    std::cout << std::left;
  }
  else if(alignement == 'r')
  {
    std::cout << std::right;
  }
  else
  {
    std::cout << std::internal;
  }

  afficher_ligne_separatrice(separateur_ligne, largeur_ligne);
  afficher_en_tete(donnees, largeur_champ, separateur_champs, marge_separateur);
  afficher_ligne_separatrice(separateur_ligne, largeur_ligne);
  afficher_tuples(donnees, largeur_champ, separateur_champs, marge_separateur);
  afficher_ligne_separatrice(separateur_ligne, largeur_ligne);
  std::cout << std::endl;
  std::cout << "L'exécution de la requête SQL ";
  std::cout << "a retourné " << PQntuples(donnees) << " enregistrement" << (PQntuples(donnees) > 1 ? 's' : '\0') << "." << std::endl;
}



#include "affichage.h"
#include<iostream>
#include<iomanip>
#include<cstring>

using namespace std;

const char barre = '|', espace = ' ';

void ligneSeparatrice(int longueur)
{
  for(int k = 0; k < longueur; k++)
  {
    cout << "-";
  }
}

void affichageConnexion(PGconn *connexion)
{
  cout << "La connexion au serveur de base de données '" << PQhost(connexion) << "' a été établie avec les paramètres suivants : " << endl;
  cout << endl << "* Utilisateur : " << PQuser(connexion) << endl;
  cout << "* Mot de passe : ";

  for(int i = 0; i < strlen(PQpass(connexion)); i++)
  {
    cout << "*";
  }

  cout << endl << "* Base de données : " << PQdb(connexion) << endl;
  cout << "* Port TCP : " << PQport(connexion) << endl;
  cout << "* Chiffrement SSL : ";

  if(PQconnectionUsedPassword(connexion) == 1)
  {
    cout << "true" << endl;
  }
  else
  {
    cout << "false" << endl;
  }

  cout << "* Encodage : " << PQparameterStatus(connexion, "server_encoding") << endl;
  cout << "* Version du protocole : " << PQprotocolVersion(connexion) << endl;
  cout << "* Version du serveur : " << PQparameterStatus(connexion, "server_version") << endl;
  cout << "* Version de la bibliothèque 'libpq' du client : " << PQlibVersion() << endl;
}

void affichageNomChamp(PGresult *res, int plusGrandChamp)
{
  for(int k = 0; k < PQnfields(res); k++)
  {
    cout << barre << espace << left << setw(plusGrandChamp) << setfill(' ') << PQfname(res, k) << espace;
  }
}

void affichageDonnee(PGresult *res, int plusGrandChamp)
{
  for(int j = 0; j < PQntuples(res); j++)
  {
    for(int k = 0; k < PQnfields(res); k++)
    {
      if(PQgetlength(res, j, k) < plusGrandChamp)
      {
        cout << barre << espace << left << setw(plusGrandChamp) << setfill(' ') << PQgetvalue(res, j, k) << espace;
      }
      else
      {
        char *champ = PQgetvalue(res, j, k);

        for(int i = 3; i >= 1; i--)
        {
          champ[plusGrandChamp - i] = '.';
        }

        champ[plusGrandChamp] = 0;
        cout << barre << espace << left << setw(plusGrandChamp) << setfill(' ') << champ << espace;
      }
    }

    cout << barre << endl;
  }
}

